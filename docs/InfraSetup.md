#### About the product or website:
In “MyClientReviews” project, as a consumer, we all rely on reviews to help us choose which company to deal with. Then as a small business owner, we were using a major review platform (who will remain nameless to protect the guilty) to help consumers know that they could have confidence in who they were dealing with. 

#### General overview of the project:
### 1
In this project, I've been actively involved in three environments: development (dev), staging (stage), and production (prod). This is a MFVN stack-based application, and throughout the development process, I utilized various tools. Nginx served as a reverse proxy for hosting the web application, MongoDB was chosen as the database, and GoDaddy served as our domain name provider. In terms of AWS services, I extensively worked with EC2, ECS, ECR, S3, IAM, and AMI.
### 2
 This product describes a setup for a software project with multiple environments and infrastructure components in the context of GitHub, AWS (Amazon Web Services), and ECS (Elastic Container Service) and the architecture assumes a containerized environment using AWS ECS for hosting the services. Here's a general overview of the components and their roles:

### GitHub Repo:
## Codebase: 
        The codebase for the project is hosted on the GitHub repository which contains the application code and devops-related files.

### Environments:
     There are three primary environments within this project:

### Development (Dev): 
        This environment corresponds to the "dev" branch in the GitHub repository. Commits to this branch trigger deployment to the development environment.
### Staging (Stage): 
          The staging environment is associated with the "stage" branch in the GitHub repository. Changes made to this branch initiate deployments to the staging environment.
### Production: 
            The production environment is connected to the "master" branch in the repository. However, deployments to the production environment are managed separately and directly within AWS ECS.

### Environment Variables:
           For Dev and Stage environments, environment variables are configured in two ways:
           Some environment variables are specified in the relevant GitHub Actions workflow YAML configuration files within the ".github/workflows" directory.
           Sensitive environment variables are stored as repository secrets in GitHub, for example, secrets.PROD_S3_SECRET_ACCESS_KEY.

### GitHub Actions:
          GitHub Actions are configured within the ".github/workflows" directory to automate the deployment process for the Dev and Stage environments. They define the workflow for building and deploying the application based on commits to the respective branches.

### AWS EC2 Instances (MCR Account):
## Linux Bastion:
       The Linux Bastion host serves as a secure gateway for accessing resources in the AWS infrastructure, including the production database and ECS services. It acts as an intermediary for secure and controlled access. The EC2 instance hosting the production database container should have its security group configured only to allow incoming connections from the Linux Bastion host. This ensures that the database is not directly exposed to the internet and is accessible only through the Bastion host.
## mcr_mongo_server:
        This virtual machine hosts the production MongoDB container. MongoDB is a NoSQL database commonly used for storing application data.

### mcr_prod_server:
## NGINX: 
       This VM hosts an NGINX server. NGINX is a web server and reverse proxy server used to handle web traffic. In this case, it resolves domain names for the production and staging environments,such as "myclientreviews.com.au"and"stage.myclientreviews.com.au," respectively.


  ## Frontend and Backend Containers for Stage:
           This server hosts frontend and backend containers for the staging environment. These containers likely run the web application in the staging environment.    


### mcr_dev_server:
## Frontend and Backend Containers for Dev:
              This VM hosts the frontend and backend containers for the development environment. It allows developers to work on and test the application in a controlled environment.
## MongoDB Container: 
           It hosts the MongoDB container used for both the dev and stage environments. This shared database container may facilitate testing and development across both environments.
## mcr_dev_2: 
         This is a separate virtual machine used for remote development purposes. Developers may use this machine to work on the application code or perform tasks related to the project.

### AWS ECS (MCR Account):
## mcrprodcluster Cluster: 
             This ECS cluster is designed to host the production services of the application. AWS ECS is a container orchestration service that allows for the deployment, management, and scaling of containerized applications.
## mcr_front_service: 
         This ECS service is responsible for running tasks associated with the frontend container in the production environment. It ensures that the frontend part of the application is available and scalable in the production environment.
## mcr_back_service: 
           Similarly, this ECS service handles tasks related to the backend containers in the production environment. It ensures that the backend components of the application are operational and scalable.


In summary, this infrastructure setup indicates a web application hosted on AWS ECS with three different environments for development, staging, and production. GitHub Actions are used for automated deployments in development and staging environments, while production deployments are managed directly within AWS ECS. AWS EC2 instances serve various purposes, such as hosting containers, and databases, and acting as secure gateways for remote access. The overall architecture appears to be designed for scalability, security, and efficient development and testing processes.

### DNS Management for our project:
DNS, or Domain Name System, is a critical component of the internet that translates human-friendly domain names into IP addresses, which computers use to identify and communicate with each other. In simpler terms, it's like a phonebook for the internet, allowing you to access websites and online services by typing in familiar domain names like "www.example.com" instead of remembering complex IP addresses like "192.168.1.1."

DNS works by storing records (DNS entries) that map domain names to IP addresses. Some common types of DNS records include:


- **A Record: Associates a domain name with an IPv4 address.**
- **AAAA Record: Associates a domain name with an IPv6 address.**
- **CNAME Record: Creates an alias for another domain name.**
- **MX Record: Specifies mail servers responsible for receiving email for a domain.**
- **TXT Record: Holds arbitrary text information about a domain.**
- **NS Record: Indicates the authoritative name servers for a domain.**
- **SOA Record: Stores information about the domain's primary DNS server and other administrative details.**

To manage DNS entries for a domain registered with GoDaddy, you would typically use their domain management platform, which provides a user-friendly interface for making changes. Here's a brief overview of how to manage DNS entries in GoDaddy:


### Log in to your GoDaddy Account: 
Go to the GoDaddy website and log in with your account credentials.

### Access Your Domain Management: 
After logging in, go to your account dashboard and find the domain you want to manage. Click on it to access the domain settings.

### Go to DNS Management: 
Within your domain settings, look for an option called "DNS" or "DNS Management." Click on it.

### View and Edit DNS Records: 
In the DNS Management section, you will see a list of your current DNS records. You can add, edit, or delete DNS records here. To add a new record, there is typically an option to "Add Record" or "Add DNS Entry." You'll need to select the type of record you want to add and provide the necessary information.

### Save Changes: 
After adding or editing DNS records, make sure to save your changes. This usually involves clicking a "Save" or "Update" button.
Propagation Time: DNS changes can take some time to propagate across the internet. It can vary from a few minutes to 48 hours or more. During this period, some users may see the old DNS information, while others see the new settings.

Remember that making incorrect changes to your DNS settings can disrupt your website and email services, so it's essential to be cautious and double-check the changes you make. If you're not confident in managing DNS yourself, you can always reach out to GoDaddy's support or seek professional assistance to avoid potential issues.

### MCR Project domain provider is?
In this project, we used GoDaddy to configure the DNS for myclientreviews.com.au domain.

### Where to manage the DNS of MCR websit and project:
We use Godaddy to manage the DNS.

#### Slack
Slack channel mcr-production-support and mcr_test_alert are used for reporting new and resolved statuses of severity 1 outages.
          