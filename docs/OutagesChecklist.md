# My Client Reviews Outage Checklist
## General action items
- Try restarting nginx on mcr_prod_server;
- Check logs on ECS tasks on mcrprodcluster;
- If there are any issues in the recent logs, try restarting the tasks (or starting new revisions) for either or both mcr_front/mcr_back;
- Check if the DB container is down on mcr_mongo_server, if so, back it up and try restarting the container;
- If the issue cannot be resolved within 2 hours. set-up a temporary failover machine (i.e. starting up a new standalone VM, deploying the live project there, and redirecting traffic there via either Nginx or registrar’s DNS);
- Share any error logs and in the Slack channel;
- Upon trying any of the items here, make a note of that in Slack and report whether it seems to resolve the issue or not. 

## Some specific issues
- Something is down after a new release: try rolling back to an older build, i.e. pushing earlier master branch builds via GitHub actions.
- There are issues with Stripe/SendGrid API usage abuse (e.g. emails start going out uncontrollably or system starts spamming Stripe API): redeploy a revision of mcr_back with removed API keys or switch to a temp failover machine.  
- SSL expired: attempt to update the certificate.
- Reported data loss: try to restore DB backup if available.  