# My Client Reviews Critical Functionality Checklist
## Login
- Verify that the [login page](https://myclientreviews.com.au/login) is accessible (e.g., via a web browser).
- Confirm that the interface elements (fields for username/email and password, login button) are present and properly labeled.
- Ensure that valid credentials allow the user to successfully log in.
- Check that invalid credentials result in an appropriate error message.

## Upload CSV for Invitation
- Go to the [get reviews](https://myclientreviews.com.au/get-reviews) page, and select `Invite Clients by CSV`.
- Confirm that the interface to upload a CSV file is clearly accessible post-login.
- Upload a test CSV file, like:
```csv
Name,Email
test,admin@quorum.ltd
```
- Ensure that file selection dialog works and allows for the selection of CSV files specifically.
- Validate that the system successfully accepts and processes a correctly formatted CSV file.
- Test the feedback provided to the user after successful/unsuccessful upload (e.g., success message, progress indicator, error message).
- Evaluate the performance and timeout thresholds with CSVs of varying sizes.
- Send out the user invitations.

## Email with Review Invite & Working Link
- Check that an email is promptly sent to the addresses listed in the uploaded CSV.
- Validate that the email contains the correct and expected content, including any personalization using the CSV data.
- Ensure that the email’s subject line is appropriately descriptive and contains no placeholders or incorrect data.
- Confirm that the email includes a review invite link that is unique and secure for each recipient.
- Test the functionality of the review invite link in various email clients and web browsers.
- Verify that the link leads to the expected landing page and that the review submission process functions as intended.