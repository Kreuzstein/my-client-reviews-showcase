# My Client Reviews Deployment Process
## Preparation
1. **Feature Freeze and Testing**
	1. Finalise the set of features and bug fixes to be included in the release.
	2. Push the release version to the staging branch.
	3. List the key updated items that should be tested and share it with all stakeholders.
	4. Schedule and confirm the release time and date.
	5. If no objections or change requests arise in before, proceed with the release.
2. **Create a Release PR**:
	1. Create a pull request dedicated to this release merging the stage branch into master.
3. **Update Change log**:
	1. Update documentation with any new features, deprecation, or breaking changes.
	2. Maintain a `CHANGELOG.md` that summarises the changes in the new release.

## **Deployment:** 
4. **Manual Database Backup**:
	1. Perform a manual backup of the production database as a safety precaution even if automated backups exist.
6. **Merge Pull Request & Deploy to Production**:
	1. Review the pull request to ensure all commits are intended for release and that Continuous Integration checks pass.
	2. Approve and merge the pull request into the main branch, then begin the deployment process.
	3. The deployment should is automated with CI/CD pipelines that handle the deployment once the pull request is merged.
7. **Monitor Deployment**:
	1. Monitor the deployment process for any issues.
	2. Deployment should be done during off-peak hours to minimise impact, typically starting at 9 PM Sydney time.
8. **Post-deployment Testing**:
	1. Go through the `CriticalFunctionalityChecklist.md` to verify critical functionality in the production environment.
	2. Run automated smoke tests and sanity checks where possible to ensure stability.
9. **Communication**:
	1. Notify stakeholders about the start and successful completion of the deployment on `mcr-production-support` Slack channel.
	2. Communicate any new features or changes to end-users if necessary.

## **Contingency Plan:** 
10. **Handling Critical Issues**:
	1. If a critical issue is detected during deployment or testing, be prepared to execute a rollback plan if necessary.
	2. Rollback could involve restoring the previous database backup and redeploying the previous application version.
12. **Post-mortem Analysis**:
	1. In case of a severe issue, conduct a post-mortem to understand what went wrong and document any lessons learned.

## **Support:** 
12. **Availability for Immediate Fixes**:
	1. Be available to address any immediate issues after the release.
	2. If not available, ensure that there is clear documentation on how to escalate and who to contact in case of emergencies.
14. **Team Communication**:
	1. If you require assistance during the deployment, communicate clearly with the team and provide all necessary information to resolve issues quickly on the Slack channel `mcr-devops`.