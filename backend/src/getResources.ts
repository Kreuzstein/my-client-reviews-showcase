import { FromSchema } from "json-schema-to-ts";
import { envSchema } from "./schemas/env";
import { UrlBuilder } from "./helpers/UrlBuilder";
import { MailService } from "./services/MailService";
import { ImageService } from "./services/ImageService";
import { UserService } from "./services/UserService";
import { CompanyService } from "./services/CompanyService";
import { ClientService } from "./services/ClientService";
import { ReviewService } from "./services/ReviewService";
import { InvitationService } from "./services/InvitationService";
import { BillingService } from "./services/BillingService";
import { PageService } from "./services/PageService";
import { FastifyLoggerInstance } from "fastify";
import { AWSCloudWatchLogsService } from "./services/AWSCloudWatchLogsService";
import { GoogleIndexingService } from "./services/GoogleIndexingService";
import { ReportLogService } from "./services/ReportLogService";
import { NewsletterService } from "./services/NewsletterService";
import { SmsService } from "./services/SmsService";
import { PlatformStatsService } from "./services/PlatformStatsService";
import { SalesService } from "./services/SalesService";

export interface Resources {
    urlBuilder: UrlBuilder;
    mailService: MailService;
    imageService: ImageService;
    userService: UserService;
    companyService: CompanyService;
    clientService: ClientService;
    reviewService: ReviewService;
    invitationService: InvitationService;
    billingService: BillingService;
    pageService: PageService;
    cloudWatchLogsService: AWSCloudWatchLogsService;
    googleIndexingService: GoogleIndexingService;
    reportLogService: ReportLogService;
    newsletterService: NewsletterService;
    smsService: SmsService;
    platformStatsService: PlatformStatsService;
    salesService: SalesService;
}

export function getResources(
    env: FromSchema<typeof envSchema>,
    logger: FastifyLoggerInstance
): Resources {
    const smsService = new SmsService(
        {
            apiKey: env.SMS_API,
            testMode: env.MAIL_SERVICE_TEST_MODE,
        },
        logger,
    );

    //   other service defintions here

    return {
        urlBuilder,
        mailService,
        imageService,
        userService,
        companyService,
        clientService,
        reviewService,
        invitationService,
        billingService,
        pageService,
        cloudWatchLogsService,
        googleIndexingService,
        reportLogService,
        newsletterService,
        smsService,
        platformStatsService,
        salesService,
    };
};