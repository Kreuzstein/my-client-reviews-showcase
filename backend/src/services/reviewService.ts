import { FilterQuery, Types } from "mongoose";

import { Review, ReviewDoc, ReviewProps } from "../models/Review";
import { ReviewImport } from "../models/ReviewImport";
import { Client, ClientDoc } from "../models/Client";
import { CompanyDoc } from "../models/Company";
import { ReviewSource } from "../types/common";
import { ReviewErrors } from "../types/errors";
import { ServiceError } from "./ServiceError";
import { CsvRecord as ReviewImportCsvRecord } from "routes/admin/routeOfImportReviews";

import { addDays, parse } from "date-fns";
import randToken from "rand-token";
import { UrlBuilder } from "../helpers/UrlBuilder";

interface Config {
  urlBuilder: UrlBuilder;
};

type PopulatedReviewDoc = ReviewDoc & {
  client: ClientDoc;
  company: CompanyDoc;
};

const resendReportLimitInDays = 7;

interface GetReviewsParams {
  companyId: string;
  skip?: number;
  limit?: number;
  sort?: "createdAt" | "-createdAt" | "rate" | "-rate" | "reply" | "-reply";
  withoutReported?: boolean;
  onlyPositive?: boolean;
  withReplys?: boolean;
  createdBefore?: Date;
  rating?: number;
}

interface GetReviewsQueryParams {
  filterQuery: FilterQuery<ReviewDoc>;
  skip?: number;
  limit?: number;
  sort?: "createdAt" | "-createdAt" | "rate" | "-rate" | "reply" | "-reply";
}

interface FullReviewStats {
  rateAvg: number;
  count: number;
  excellent: number;
  good: number;
  okay: number;
  poor: number;
  terrible: number;
}

interface ReviewSourceStats {
  year: number;
  byBcc: number;
  byBccPercent: number;
  byEmail: number;
  byEmailPercent: number;
  byQr: number;
  byQrPercent: number;
  bySms: number;
  bySmsPercent: number;
}

interface ReviewSourceStatsByDateRange {
  dateRangeFrom: Date;
  dateRangeTo: Date;
  byBcc: number;
  byBccPercent: number;
  byEmail: number;
  byEmailPercent: number;
  byQr: number;
  byQrPercent: number;
  bySms: number;
  bySmsPercent: number;
}

interface NewReviewsStatsGlobal {
  timeSeries: {
    time: Date;
    value: number;
  }[];
  monthly?: number;
}

interface DetlaItem {
  _id: number;
  count: number;
}

const rateAverageLimit = 3;

export class ReviewService {
  private config: Config;

  constructor(config: Config) {
    this.config = config;
  }

  async createReview({
    company,
    client,
    rate,
    message,
    source,
    suspended,
  }: Pick<
    ReviewProps,
    "company" | "client" | "rate" | "message" | "source" | "suspended"
  >): Promise<ReviewDoc> {
    const review = new Review({
      company,
      client,
      rate,
      message,
      source,
      suspended,
    });
    review.createdAt = new Date();
    await review.save();
    return review;
  }

  async getReviewById(id: string): Promise<PopulatedReviewDoc | null> {
    const query: FilterQuery<ReviewDoc> = {
      _id: Types.ObjectId(id),
      isRemoved: { $ne: true },
    };

    try {
      return (await Review.findOne(query)
        .populate("client")
        .populate("company")) as PopulatedReviewDoc;
    } catch (e) {
      return null;
    }
  }

  async getReviewByClient(
    clientId: string,
    companyId: string
  ): Promise<PopulatedReviewDoc[] | null> {
    const query: FilterQuery<ReviewDoc> = {
      client: Types.ObjectId(clientId),
      company: Types.ObjectId(companyId),
      isRemoved: { $ne: true },
      suspended: { $ne: true },
    };

    try {
      return (await Review.find(query)
        .populate("client")
        .populate("company")) as PopulatedReviewDoc[];
    } catch (e) {
      return null;
    }
  }

  async getReviews({
    companyId,
    skip = 0,
    limit = 100,
    sort = "createdAt",
    withoutReported = false,
    onlyPositive = false,
    withReplys = true,
    createdBefore,
    rating,
  }: GetReviewsParams): Promise<{
    data: PopulatedReviewDoc[];
    total: number;
  }> {
    const filterQuery: FilterQuery<ReviewDoc> = {
      company: Types.ObjectId(companyId),
      isRemoved: { $ne: true },
      suspended: { $ne: true },
    };
    if (withoutReported) {
      filterQuery.reportedAt = { $exists: false };
    }

    if (onlyPositive) {
      filterQuery.rate = { $gte: 3 };
    }
    if (createdBefore) {
      filterQuery.createdAt = {
        $lt: createdBefore,
      };
    }
    if (rating) {
      filterQuery.rate = {
        $lt: rating + 0.5,
        $gte: rating - 0.5,
      };
    }
    const sortParams: any = {};

    if (sort) {
      const sortDirection = sort.charAt(0) === "-" ? -1 : 1;
      const sortField = sort.replace("-", "");
      sortParams[sortField] = sortDirection;
      if (sort == "reply") {
        sortParams["createdAt"] = -1;
      }
    }

    const query = Review.find(filterQuery)
      .populate("client")
      .populate("company")
      .sort(sortParams)
      .skip(skip)
      .limit(limit);

    if (!withReplys) {
      query.select({ reply: false });
    }

    const data = (await query.exec()) as PopulatedReviewDoc[];
    const total = await Review.countDocuments(filterQuery);

    return { data, total };
  }

  async getReviewsSources(companyId: string): Promise<ReviewSourceStats[]> {
    const aggregation = (await Review.aggregate([
      {
        $match: {
          company: Types.ObjectId(companyId),
          isRemoved: { $ne: true },
          reportedAt: { $exists: false },
          suspended: { $ne: true },
        },
      },
      {
        $group: {
          _id: {
            year: { $year: "$createdAt" },
          },
          year: { $first: { $year: "$createdAt" } },
          byBcc: {
            $sum: { $cond: [{ $eq: ["$source", ReviewSource.byBcc] }, 1, 0] },
          },
          byQr: {
            $sum: { $cond: [{ $eq: ["$source", ReviewSource.ByQr] }, 1, 0] },
          },
          byEmail: {
            $sum: { $cond: [{ $eq: ["$source", ReviewSource.byEmail] }, 1, 0] },
          },
          bySms: {
            $sum: { $cond: [{ $eq: ["$source", ReviewSource.bySms] }, 1, 0] },
          },
        },
      },
    ])) as ReviewSourceStats[];

    aggregation.forEach((group) => {
      const groupSum = group.byBcc + group.byEmail + group.byQr + group.bySms;

      group.byBccPercent = Math.round((group.byBcc / groupSum) * 100);
      group.byEmailPercent = Math.round((group.byEmail / groupSum) * 100);
      group.byQrPercent = Math.round((group.byQr / groupSum) * 100);
      group.bySmsPercent = Math.round((group.bySms / groupSum) * 100);

      while (
        group.byBccPercent + group.byEmailPercent + group.byQrPercent + group.bySmsPercent >
        100
      ) {
        const max = Math.max(
          group.byBccPercent,
          group.byEmailPercent,
          group.byQrPercent,
          group.bySmsPercent,
        );
        switch (max) {
          case group.byBccPercent: {
            group.byBccPercent--;
            break;
          }
          case group.byEmailPercent: {
            group.byEmailPercent--;
            break;
          }
          case group.byQrPercent: {
            group.byQrPercent--;
            break;
          }
          case group.bySmsPercent: {
            group.bySmsPercent--;
            break;
          }
        }
      }
    });

    return aggregation;
  }

  async getReviewsSourcesByDateRange(
    companyId: string,
    dateRangeFrom: Date,
    dateRangeTo: Date
  ): Promise<ReviewSourceStatsByDateRange> {
    const reviewStats = (
      await Review.aggregate([
        {
          $match: {
            company: Types.ObjectId(companyId),
            isRemoved: { $ne: true },
            suspended: { $ne: true },
            reportedAt: { $exists: false },
            createdAt: { $gte: dateRangeFrom, $lte: addDays(dateRangeTo, 1) },
          },
        },
        {
          $group: {
            _id: null,
            byBcc: {
              $sum: { $cond: [{ $eq: ["$source", ReviewSource.byBcc] }, 1, 0] },
            },
            byQr: {
              $sum: { $cond: [{ $eq: ["$source", ReviewSource.ByQr] }, 1, 0] },
            },
            byEmail: {
              $sum: { $cond: [{ $eq: ["$source", ReviewSource.byEmail] }, 1, 0] },
            },
            bySms: {
              $sum: { $cond: [{ $eq: ["$source", ReviewSource.bySms] }, 1, 0] },
            },
          },
        },
        {
          $addFields: {
            dateRangeFrom: dateRangeFrom,
            dateRangeTo: dateRangeTo,
          },
        },
      ])
    ).pop() as ReviewSourceStatsByDateRange;

    if (!reviewStats) {
      return {
        dateRangeFrom: dateRangeFrom,
        dateRangeTo: dateRangeTo,
        byBcc: 0,
        byBccPercent: 0,
        byEmail: 0,
        byEmailPercent: 0,
        byQr: 0,
        byQrPercent: 0,
        bySms: 0,
        bySmsPercent: 0,
      };
    }

    const groupSum = reviewStats.byBcc + reviewStats.byEmail + reviewStats.byQr + reviewStats.bySms;

    reviewStats.byBccPercent = Math.round((reviewStats.byBcc / groupSum) * 100);
    reviewStats.byEmailPercent = Math.round((reviewStats.byEmail / groupSum) * 100);
    reviewStats.byQrPercent = Math.round((reviewStats.byQr / groupSum) * 100);
    reviewStats.bySmsPercent = Math.round((reviewStats.bySms / groupSum) * 100);

    while (
      reviewStats.byBccPercent +
      reviewStats.byEmailPercent +
      reviewStats.byQrPercent +
      reviewStats.bySmsPercent >
      100
    ) {
      const max = Math.max(
        reviewStats.byBccPercent,
        reviewStats.byEmailPercent,
        reviewStats.byQrPercent,
        reviewStats.bySmsPercent,
      );
      switch (max) {
        case reviewStats.byBccPercent: {
          reviewStats.byBccPercent--;
          break;
        }
        case reviewStats.byEmailPercent: {
          reviewStats.byEmailPercent--;
          break;
        }
        case reviewStats.byQrPercent: {
          reviewStats.byQrPercent--;
          break;
        }
        case reviewStats.bySmsPercent: {
          reviewStats.bySmsPercent--;
          break;
        }
      }
    }

    return reviewStats;
  }

  private densify(deltaArray: DetlaItem[], bounds: number[]) {
    const res: DetlaItem[] = [];
    for (let i = bounds[0]; i < bounds[1]; i++) {
      let newItem = {
        _id: i,
        count: 0
      };
      for (const populatedItem of deltaArray) {
        if (populatedItem._id === i) {
          newItem.count = populatedItem.count;
          break;
        }
      }
      res.push(newItem);
    }
    return res;
  }

  async inferNewReviewsStatsGlobal(
    dateFrom: Date,
    dateTo: Date,
    stepDays: number,
  ): Promise<NewReviewsStatsGlobal> {
    // Query nuber of reviews daily
    const stepMs = stepDays * 24 * 60 * 60 * 1000;
    const bounds = [
      Math.floor(Number(dateFrom) / stepMs),
      Math.floor(Number(dateTo) / stepMs),
    ]
    const newReviews = (
      await Review.aggregate([
        {
          $match: {
            createdAt: { $gte: dateFrom, $lte: dateTo },
          },
        },
        {
          $group: {
            _id: {
              $floor: { $divide: [{ $toLong: "$createdAt" }, stepMs] },
            },
            count: {
              $sum: 1
            }
          }
        }
      ])
    );
    const densifiedNewReviews = this.densify(newReviews, bounds);
    const timeSeries = densifiedNewReviews.map(item => {
      return {
        time: new Date(item._id * stepMs),
        value: item.count || 0,
      };
    });

    return { timeSeries };
  }

  async addReviewImportRequests(
    records: ReviewImportCsvRecord[]
  ): Promise<{
    rateAvg: number,
    count: number,
    excellent: number,
    good: number,
    okay: number,
    poor: number,
    terrible: number,
    batchId: string,
  }> {
    let count = 0;
    let rateTotal = 0;
    let excellent = 0;
    let good = 0;
    let okay = 0;
    let poor = 0;
    let terrible = 0;

    const batchId = randToken.generate(16);

    for (const row of records) {
      const rate = Number(row.rate);
      count++;
      rateTotal += rate;

      switch (true) {
        case rate >= 4.5:
          excellent++;
          break;

        case rate >= 3.5:
          good++;
          break;

        case rate >= 2.5:
          okay++;
          break;

        case rate >= 1.5:
          poor++;
          break;

        default:
          terrible++;
          break;
      }

      const stats = {
        ...row,
        rate,
        date: parse(row.date, 'dd/MM/yy', new Date()),
        replyDate: null as Date | null,
        replyMessage: null as String | null,
      }
      if (!!row["reply-date"] && !!row["reply-message"]) {
        stats.replyDate = (row["reply-date"], 'dd/MM/yy', new Date());
        stats.replyMessage = row["reply-message"];
      }
      delete stats["reply-date"];
      delete stats["reply-message"];
      const token = randToken.generate(32);
      const reviewImport = new ReviewImport({
        createdAt: new Date(),
        batchId,
        token,
        ...stats,
      });
      await reviewImport.save();
    }

    const asPercent = (tally: number): number => {
      return Math.round((tally / count) * 100);
    };

    return {
      count,
      rateAvg: Number((rateTotal / count).toFixed(2)),
      excellent: asPercent(excellent),
      good: asPercent(good),
      okay: asPercent(okay),
      poor: asPercent(poor),
      terrible: asPercent(terrible),
      batchId,
    };
  }

  async importReview(
    token: string
  ): Promise<void> {
    const reviewImport = await ReviewImport.findOne({
      token,
    });

    if (!reviewImport) throw new ServiceError({ code: ReviewErrors.TokenInvalid });

    const { company, rate, message, date: createdAt, email, name, redeemed } = reviewImport;

    if (!redeemed) {
      let clientId = '';
      const existingClient = await Client.findOne({
        company,
        email,
      });
      if (existingClient != null) {
        clientId = existingClient._id;
      } else {
        const newClient = new Client({
          email,
          name,
          company,
          createdAt: new Date(),
          lastReviewAt: createdAt,
        });
        await newClient.save();
        clientId = newClient._id;
      }

      const review = new Review({
        company,
        client: clientId,
        rate,
        message,
        suspended: false,
        createdAt,
      });

      if (!!reviewImport.replyDate && !!reviewImport.replyMessage) {
        review.reply = {
          createdAt: reviewImport.replyDate,
          message: reviewImport.replyMessage,
        }
      }

      await review.save();

      reviewImport.redeemed = true;
      reviewImport.markModified("redeemed");
      await reviewImport.save();
    }
    return;
  }

//   other review methods go here
}