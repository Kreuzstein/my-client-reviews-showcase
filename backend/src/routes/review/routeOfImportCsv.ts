import { FastifyPluginAsync } from "fastify";
import { FromSchema } from "json-schema-to-ts";
import fastifyMultipart, { MultipartFile } from "fastify-multipart";

import { UserDoc } from "../../models/User";
import { UserRole } from "../../types/common";
import { ServiceError } from "../../services/ServiceError";
import { AdminErrors, ClientErrors } from "../../types/errors";

import csvParse from "csv-parse/lib/sync";

type CsvRecordGeneric = { [key: string]: string };

export interface CsvRecord {
  name: string;
  email: string;
  rate: string;
  date: string;
  message: string;
  source: string;
  "reply-date"?: string;
  "reply-message"?: string;
};

const schema = {
  body: {
    type: "object",
    required: ["csv"],
    properties: {
      csv: { $ref: "#fileSchema" },
    },
  },
  response: {
    200: {
      type: "object",
      required: ["rateAvg", "count",],
      properties: {
        rateAvg: {
          type: "number",
        },
        count: {
          type: "number",
        },
        excellent: {
          type: "number",
        },
        good: {
          type: "number",
        },
        okay: {
          type: "number",
        },
        poor: {
          type: "number",
        },
        terrible: {
          type: "number",
        },
        batchId: {
          type: "string",
        },
      },
    },
  },
} as const;

export const routeOfImportCsv: FastifyPluginAsync = async (fastify) => {
  fastify.register(fastifyMultipart, {
    attachFieldsToBody: true,
    sharedSchemaId: "#fileSchema",
    limits: {
      fileSize: 100 * 1024,
      files: 1,
    },
  });
  fastify.route<{ Body: FromSchema<typeof schema.body> }>({
    method: "POST",
    url: "/import-reviews",
    schema,
    preHandler: fastify.auth([fastify.verifyAuth]),
    async handler(request, reply) {
      const user = request.user as UserDoc; // Because we called verifyAuth prehandler
      if (user.role != UserRole.manager && user.role != UserRole.support) {
        return reply.code(403).send({ code: AdminErrors.NotManager });
      }

      const csv = request.body.csv as MultipartFile;
      const csvBuffer = await csv.toBuffer();

      let records: CsvRecord[];
      try {
        const recordForChecks = csvParse(csvBuffer) as CsvRecordGeneric[];
        const expectedHeaders = [
          "name",
          "email",
          "rate",
          "date",
          "message",
          "source",
          "reply-message",
          "reply-date",
        ];
        const { success: headerChecksuccess, error: headerCheckError, data: headerCheckData } = checkHeaders(
          expectedHeaders,
          Object.values(recordForChecks[0]),
        );
        if (!headerChecksuccess) {
          return reply.code(400).send({ code: headerCheckError, data: headerCheckData });
        }
        records = csvParse(csvBuffer, {
          columns: expectedHeaders,
        }) as CsvRecord[];
        console.log(records);
      } catch {
        return reply.status(400).send({ code: ClientErrors.CsvInvalid });
      }

      if (existRecordsHeader(records[0])) {
        records.shift();
      }

      if(!checkRate(records)) {
        return reply.code(400).send({ code: AdminErrors.InvalidRate});
      }

      if(!checkValidDate(records)) {
        return reply.code(400).send({ code: AdminErrors.InvalidDate});
      }

      if(!checkValidReply(records)) {
        return reply.code(400).send({ code: AdminErrors.InvalidReplyDate});
      }
      
      if (records.length > 500) {
        return reply.code(400).send({ code: ClientErrors.ImportLimitExceeded });
      }

      try {
        return await fastify.resources.reviewService.addReviewImportRequests(
          records
        );
      } catch (e) {
        if (e instanceof ServiceError) {
          if (e.code == ClientErrors.ClientDuplicated) {
            return reply
              .code(400)
              .send({ code: ClientErrors.ClientDuplicated });
          }
        }
        fastify.log.error(e, ClientErrors.ClientUnhandledError);
        return reply
          .code(500)
          .send({ code: ClientErrors.ClientUnhandledError });
      }
    },
  });
};

function existRecordsHeader(header: CsvRecord) {
  return !(
    header.name.toLowerCase() !== "name" ||
    header.email.toLowerCase() !== "email" ||
    header.rate.toLowerCase() !== "rate" ||
    header.date.toLowerCase() !== "date" ||
    header.message.toLowerCase() !== "message" ||
    header.source.toLowerCase() !== "source"
  );
}

const checkHeaders = (expectedHeaders: string[], parsedHeaders: string[]) => {
  const result = {
    success: true,
    error: undefined as string | undefined,
    data: [] as string[],
  };
  for (const headerToCheck of expectedHeaders) {
    if (!parsedHeaders.map((item) => item.toLowerCase()).includes(headerToCheck.toLowerCase())) {
      result.success = false;
      result.error = AdminErrors.MissingHeaders
      result.data.push(headerToCheck);
    }
  }
  return result;
} 

const checkRate = (parsedArray: CsvRecord[]) => {
  for (const { rate } of parsedArray) {
    if (Number(rate) < 1 || Number(rate) > 5 ) return false;
  }
  return true;
}

const checkValidDate = (parsedArray: CsvRecord[]) => {
  for (const { date } of parsedArray) {
    if (!(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/([0-9]{2})$/.test(date))) return false;
  }
  return true;
}

const checkValidReply = (parsedArray: CsvRecord[]) => {
  for (const { "reply-date": date, "reply-message": message } of parsedArray) {
    if (!date || !message) continue;
    if (!(/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/([0-9]{2})$/.test(date))) return false;
  }
  return true;
}