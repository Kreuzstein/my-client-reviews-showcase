import { FastifyPluginAsync } from "fastify";
import { routeOfImportCsv } from "./routeOfImportCsv";
// other endpoint import statements go here 

export const clientRoutes: FastifyPluginAsync = async (fastify) => {
    fastify.register(routeOfImportCsv);
    //   other endpoint registration calls here
};