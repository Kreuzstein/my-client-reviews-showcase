import { Config } from "fastify-cron";
import { FastifyInstance } from "fastify";
import path from "path";
const fs = require("fs");

import { BillingErrors, UserErrors } from "./types/errors";
import { EmailTemplateName, SubscriptionStatus, emailAddressTypes } from "./types/common";

import { Review } from "./models/Review";
import { Company, CompanyDoc } from "./models/Company";
import { User, UserDoc } from "./models/User";
import { Client } from "./models/Client";
import { BusinessCategory } from "./models/BusinessCategory";

import { ServiceError } from "./services/ServiceError";
import { addMinutes, addYears } from "date-fns";
import subMonths from "date-fns/subMonths";
import csvParse from "csv-parse/lib/sync";
import randToken from "rand-token";
import {
  CloudWatchLogsClient,
  GetQueryResultsCommand,
  GetQueryResultsResponse,
  QueryStatus,
  StartQueryCommand,
} from "@aws-sdk/client-cloudwatch-logs";
import { companyCategoriesFull } from "./companyCategoriesFull";
import { Email as EmailRecord } from "./models/Email";

interface CsvReviewRecord {
  reviewDate: Date;
  name: string;
  rate: string;
  message: string;
  replyDate: Date;
  replyMessage: string;
}

const delayToUpdateOwnerData = 30;
const ownerUpdaterCronTime = addMinutes(new Date(), delayToUpdateOwnerData);
const ownerUpdaterMinutes = ownerUpdaterCronTime.getMinutes();
const ownerUpdaterHour = ownerUpdaterCronTime.getHours();

export const cronJobsConfig: Config = {
  jobs: [
    {
      name: "sendReviewInviteSms",
      cronTime: "*/5 * * * *",
      // @ts-ignore
      async onTick(fastify: FastifyInstance): Promise<void> {
        const testMode = process.env.MAIL_SERVICE_TEST_MODE == "true";
        if (testMode) {
          fastify.log.info("sendReviewInviteSms. Test mode is enabled. SMS sending skipped.");
          return;
        }
        const invites =
          await fastify.resources.invitationService.getReviewInviteSms(100);

        if (!invites.length) {
          fastify.log.info("sendReviewInviteSms. No review invite sms to be sent.");
          return;
        }

        fastify.log.info(`sendReviewInviteSms. ${invites.length} SMS invites detected.`);
        const markInviteAsInvalid = async (invite: typeof invites[0]) => {
          invite.invalid = true;
          await invite.save();
        };

        const companiesToSkip: string[] = [];

        for (const invite of invites) {
          fastify.log.info(`Debug: ${JSON.stringify(companiesToSkip)}`);
          // Check for SMS limit
          const companyId = invite.company._id;
          if (companiesToSkip.includes(companyId)) continue;
          const { smsAllowed } = await fastify.resources.companyService.checkSmsLimit(companyId);
          if (!smsAllowed) {
            companiesToSkip.push(companyId);
            continue;
          }
          const phone = invite.client?.phone;
          if (!phone) {
            markInviteAsInvalid(invite);
            continue;
          };
          const shorthand = invite.reviewToken?.shorthand;
          if (!shorthand) {
            markInviteAsInvalid(invite);
            continue;
          };

          const companyNameShort = (() => {
            let name = invite.company.name;
            if (name.length > 20) {
              name = name.split(" ")[0] + " " + name.split(" ")[1] + " " + name.split(" ")[2];
            }
            if (name.length > 18) {
              name = name.slice(0, 18) + "..";
            }
            return name;
          })();
          const companyNameShortEr = (() => {
            let name = companyNameShort;
            // Only allow alphanumeric values and whitespace + make sure no duplicate spaces are there
            name = name
              .replace(/[^a-zA-Z0-9 ]/g, '')
              .replace(/\s{2,}/g, ' ');
            if (name.length > 11) {
              name = name.slice(0, 11);
            }
            return name;
          })();
          const body = `Thank you for doing business with ${companyNameShort}.\nWe would really appreciate you leaving a review for us at myclientreviews.com.au/r/${shorthand}\nNo reply.`;
          try {
            await fastify.resources.smsService.sendSms(
              body,
              [phone],
              companyNameShortEr,
            );
            invite.sent = true;
            await invite.save();
            await fastify.resources.companyService.decrementSmsBalance(companyId);
            continue;
          } catch (e) {
            fastify.log.warn(
              `ERROR_sendReviewInviteSms. Couldn't send the invite for the review token ${invite.reviewToken.value}. Details: ${(e as Error).message}}`
            )
          }
        };
      }
    },
    {
      name: "refreshFreeSmsQuota",
      cronTime: "0 0 1 * *",
      // @ts-ignore
      async onTick(fastify: FastifyInstance): Promise<void> {
        fastify.log.info("refreshFreeSmsQuota. Started.");
        try {
          await fastify.resources.companyService.refreshFreeSmsQuota();
          return;
        } catch (e) {
          fastify.log.warn(
            `ERROR in refreshFreeSmsQuota. Details: ${(e as Error).message}}`
          )
        }
      }
    },
    {
      name: "checkForConvertedLeads",
      cronTime: "15 3 * * *",
      async onTick(fastify: FastifyInstance): Promise<void> {
        try {
          fastify.log.info("checkForConvertedLeads. Started.");
          const users = await User.find({
            isTest: { "$ne": true },
            isEmailVerified: true,
          });
          fastify.log.info(`checkForConvertedLeads. Detected ${users.length} live users.`);
          if (!users) return;
          const promises: Promise<any>[] = []
          for (const { email, _id } of users) {
            promises.push((async () => {
              const { detectedAsLead } = await fastify.resources.mailService.markNewUserAsConvertedLead(email);
              if (!detectedAsLead) return;
              fastify.log.info(`checkForConvertedLeads. ${email} is a newly converted lead.`);
              const logsToMigrate = await fastify.resources.mailService.auditLogHistoryForMigration(email);
              if (!logsToMigrate) return;
              fastify.log.info(`checkForConvertedLeads. Detected ${logsToMigrate.length} non-automated audit logs. Attempting to migrate.`);
              const company = await Company.findOne({ user: _id });
              if (!company) return;
              await fastify.resources.reportLogService.importReportLogs(logsToMigrate, company._id);
              return;
            })());
          }
          await Promise.allSettled(promises);
        } catch (e) {
          fastify.log.warn(
            `ERROR_checkForConvertedLeads. ${JSON.stringify(e)}`
          );
        }
      }
    },
    // other cron jobs go here
  ],
};