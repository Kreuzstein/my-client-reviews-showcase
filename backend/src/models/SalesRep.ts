import {
    createSchema,
    ExtractDoc,
    ExtractProps,
    Type,
    typedModel,
} from "ts-mongoose";
import { normalizeForReplyPlugin } from "../helpers/normalizeForReply";
import { isAfter, subMonths } from "date-fns";
const required = true;
const unique = true;

export const schema = createSchema({
    name: Type.string({ required }),
    email: Type.string({ required, unique }),
    createdAt: Type.date({ required }),
    archived: Type.boolean(),
    archivedDate: Type.date(),
    conversion: Type.array({ required }).of({
        companyId: Type.string({ required }),
        date: Type.date({ required }), // date is used for the compuited field `conversionRates`, hence we can't just have paymentId only 
        month: Type.number({ required }), // first/second/third month as we only need to track if the converted customer stayed for up to 3 mos 
        paymentId: Type.string({ required }),
    }),
    signups: Type.array({ required }).of({
        companyId: Type.string({ required }),
        date: Type.date({ required }),
    }),
    refToken: Type.string({ required }),
    uploads: Type.array().of({
        uploadDate: Type.date({ required }),
        data: Type.string({ required }),
        filename: Type.string({ required }),
    }),
    reports: Type.array().of({
        createdDate: Type.date({ required }),
        data: Type.string({ required }),
        filename: Type.string({ required }),
    }),
    ...({} as {
        generatedField: string;
        signupsCount: () => {
            lastMonth: number,
            allTime: number,
        };
    }),
    ...({} as {
        generatedField: string;
        conversionRates: () => {
            lastMonth: number,
            allTime: number,
        };
    }),
});
schema
    .virtual("signupsCount")
    .get(function signupsCount(this: SalesRepDoc): {
        lastMonth: number,
        allTime: number,
    } {
        const res = {
            lastMonth: 0,
            allTime: 0,
        };
        if (!this.signups || !this.signups.length) return res;
        res.allTime = this.signups.length;
        res.lastMonth = this.signups.filter((item) => isAfter(item.date, subMonths(new Date(), 1))).length;
        return res;
    });
schema
    .virtual("conversionRates")
    .get(function conversionRates(this: SalesRepDoc): {
        lastMonth: number,
        allTime: number,
    } {
        const res = {
            lastMonth: 0,
            allTime: 0,
        };
        if (!this.conversion || !this.conversion.length || !this.signups || !this.signups.length) return res;
        res.allTime = (this.conversion.filter((item) => item.month === 1).length / this.signups.length) ?? 0;
        const lastMonthSignups = this.signups.filter((item) => isAfter(item.date, subMonths(new Date(), 1))).length;
        if (!lastMonthSignups) return res;
        res.lastMonth = (
            this.conversion.filter((item) => item.month === 1 && isAfter(item.date, subMonths(new Date(), 1))).length /
            lastMonthSignups
        ) ?? 0;
        return res;
    });
schema.plugin(normalizeForReplyPlugin);

export const SalesRep = typedModel("SalesRep", schema, "salesReps");
export type SalesRepDoc = ExtractDoc<typeof schema>;
export type SalesRepProps = ExtractProps<typeof schema>;