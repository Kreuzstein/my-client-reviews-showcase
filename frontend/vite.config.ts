import path from "path";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import viteSSR from "vite-ssr/plugin.js";
import ViteRadar from "vite-plugin-radar";
import viteCompression from 'vite-plugin-compression';
import removePreloads from './src/removePreloads';

export function renderChunks(deps: Record<string, string>) {
  let chunks = {};
  Object.keys(deps).forEach((key) => {
    if (['react', 'react-router-dom', 'react-dom'].includes(key)) return;
    chunks[key] = [key];
  });
  return chunks;
}

const is_dev = process.env.NODE_ENV.trim() !== "production"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    viteSSR(),
    vue(),
    ViteRadar({
      enableDev: is_dev,
      analytics: {
        id: process.env.VITE_GOOGLE_ANALYTICS_KEY || "KEY-NO-FOUND",
      },
    }),
    viteCompression(),
    removePreloads(),
  ],
  server: {
    port: parseInt(process.env.FRONTEND_INTERNAL_PORT),
  },
  resolve: {
    alias: [
      {
        find: "@backend",
        replacement: path.resolve(__dirname, "../backend"),
      },
      {
        find: "@",
        replacement: path.resolve(__dirname, "./src"),
      },
    ],
    mainFields: ["main", "module"],

  },
  build: {
    sourcemap: false,
    rollupOptions: {

      output: {
        manualChunks: {
          axios: ['axios'],
          indexing: ['@fingerprintjs/fingerprintjs-pro'],
          bundle_v: ['vue'],
          bundle_chart: ['chart.js', '@j-t-mcc/vue3-chartjs', 'chartjs-adapter-date-fns', 'chartjs-plugin-datalabels'],
          bundle_vr: ['vue-router'],
          bundle_v3: ['@googlemaps/js-api-loader', '@nuxt/devalue', '@popperjs/core', '@vueuse/head', 'popper-max-size-modifier'],
          vendor: ['pinia', '@vueform/multiselect', 'imask', 'json-schema-to-ts', 'qrcode', 'qs', 'universal-cookie'],
          fastify: ['fastify', 'fastify-static'],
          stripe: ['@stripe/stripe-js'],
        },
        compact: true,
        inlineDynamicImports: false,
      },
    },
  },
});