import { defineStore } from "pinia";

import { BusinessCategory, BusinessCategoryQuery, BusinessCategoryType, businessCategoryTypes } from "@/types/commonTypes";
import { ServiceError } from "@/services/ServiceError";

import { useNotificationModalStore } from "@/containers/notificationModal/store";

import { showErrorNotification } from "@/helpers/errorHelper";

export const useAdminBusinessCategoriesStore = defineStore({
    id: "adminBusinessCategories",
    state: () => ({
        tier1Meta: {
            page: 1,
            pageCount: 1,
            perPage: 200,
        },
        tier2Meta: {
            page: 1,
            pageCount: 1,
            perPage: 200,
        },
        tier3Meta: {
            page: 1,
            pageCount: 1,
            perPage: 200,
        },
        businessCategories: [] as BusinessCategory[],
        tier1Categories: [] as BusinessCategory[],
        tier2Categories: [] as BusinessCategory[],
        tier3Categories: [] as BusinessCategory[],
        isLoading: false,
        tier1Selected: "",
        tier2Selected: "",
        tier3Selected: "",
        tier1Hovered: "",
        tier2Hovered: "",
        tier3Hovered: "",
        isEditModalOpen: false,
        categoryEditMeta: {
            header: "Add a New Top Category",
            editMode: false,
            type: businessCategoryTypes.tier1 as BusinessCategoryType,
            path: [] as string[],
        },
        categoryEditValue: "",
        categoryEditType: businessCategoryTypes.tier1 as BusinessCategoryType,
        categoryEditTarget: "",
        searchForm: "",
    }),
    actions: {
        async init() {
            const tier1Data =
                await this.fetchData({
                    page: 1,
                    "per-page": 50,
                    full: true,
                    filter: businessCategoryTypes.tier1
                });
            this.tier1Categories = tier1Data.data;
            this.tier1Meta = tier1Data.meta;
        },

        async fetchData(params?: BusinessCategoryQuery) {

            this.isLoading = true;
            let data = {
                data: [],
                meta: {},
            };
            try {
                data = (await this.companyService.searchCategories(params)).data;
            } catch (e) {
                if (e instanceof ServiceError) {
                    commonStore.showNotification("error", e.message);
                }
            } finally {
                this.isLoading = false;
                return data;
            }
        },

        async setTier1(value: string) {
            this.tier2Categories = []
            this.tier3Categories = []
            this.tier2Selected = "";

            this.tier1Selected = value;
            const params = {
                page: this.tier2Meta.page,
                "per-page": this.tier2Meta.perPage,
                path: encodeURI(this.tier1Selected),
                full: true,
                filter: businessCategoryTypes.tier2,
            };
            if (!!this.searchForm) {
                Object.assign(params, {
                    search: this.searchForm,
                    counters: true,
                })
            }

            const tier2Data =
                await this.fetchData(params);
            this.tier2Categories = tier2Data.data;
            this.tier2Meta = tier2Data.meta;
        },

        async setTier2(value: string) {
            this.tier3Categories = [];

            this.tier2Selected = value;
            const params = {
                page: this.tier3Meta.page,
                "per-page": this.tier3Meta.perPage,
                path: encodeURI(this.tier2Selected),
                full: true,
                filter: businessCategoryTypes.tier3,
                search: this.searchForm,
                counters: false,
            };
            if (!!this.searchForm) {
                Object.assign(params, {
                    search: this.searchForm,
                })
            }
            const tier3Data =
                await this.fetchData(params);
            this.tier3Categories = tier3Data.data;
            this.tier3Meta = tier3Data.meta;
        },

        async hoverTier1(value: string) {
            this.tier1Hovered = value;
        },

        async hoverTier2(value: string) {
            this.tier2Hovered = value;
        },

        async hoverTier3(value: string) {
            this.tier3Hovered = value;
        },

        openConfirmDeleteModal({ value, path, type }: BusinessCategory) {
            this.isEditModalOpen = false;
            this.categoryEditType = type as BusinessCategoryType;
            const notificationModalStore = useNotificationModalStore();
            notificationModalStore.openModal({
                type: "warning",
                title: "Please Confirm",
                message: `Are you sure you want to delete the '${value}' category?`,
                primaryButton: {
                    title: "OK",
                    onClick: () => {
                        void this.deleteCategory(value);
                    },
                },
                secondaryButton: {
                    title: "Cancel",
                },
            });
        },

        closeEditModal() {
            this.isEditModalOpen = false;
        },

        newTier1() {
            const type = businessCategoryTypes.tier1 as BusinessCategoryType;
            this.categoryEditMeta = {
                header: "Add a New Top Category",
                editMode: false,
                type,
                path: [] as string[],
            };
            this.categoryEditValue = "";
            this.isEditModalOpen = true;
            this.categoryEditType = type;
        },

        newTier2() {
            const type = businessCategoryTypes.tier2 as BusinessCategoryType;
            this.categoryEditMeta = {
                header: "Add a New Category",
                editMode: false,
                type,
                path: [this.tier1Selected],
            };
            this.categoryEditValue = "";
            this.isEditModalOpen = true;
            this.categoryEditType = type;
        },

        newTier3() {
            const type = businessCategoryTypes.tier3 as BusinessCategoryType;
            this.categoryEditMeta = {
                header: "Add a New Sub-category",
                editMode: false,
                type,
                path: [this.tier1Selected, this.tier2Selected],
            };
            this.categoryEditValue = "";
            this.isEditModalOpen = true;
            this.categoryEditType = type;
        },

        async createNewCategory() {
            let path = [];
            switch (this.categoryEditType) {
                case businessCategoryTypes.tier1:
                    path = []
                    break;

                case businessCategoryTypes.tier2:
                    path = [this.tier1Selected]
                    break;

                case businessCategoryTypes.tier3:
                    path = [this.tier1Selected, this.tier2Selected]
                    break;

                default:
                    break;
            }

            const newCategory = {
                value: this.categoryEditValue,
                type: this.categoryEditType,
                path,
            };

            try {
                await this.companyService.newCategory(newCategory);

                switch (this.categoryEditType) {
                    case businessCategoryTypes.tier1:
                        this.tier1Categories.push(newCategory);
                        break;

                    case businessCategoryTypes.tier2:
                        this.tier2Categories.push(newCategory);
                        break;

                    case businessCategoryTypes.tier3:
                        this.tier3Categories.push(newCategory);
                        break;

                    default:
                        break;
                }
                this.categoryEditValue = "";
                this.isEditModalOpen = false;
                commonStore.showNotification("success", "Successfully added new category");
            } catch (e) {
                showErrorNotification(e.code, "newCategory");
            }
        },

        setCategoryEditValue(value: string) {
            this.categoryEditValue = value;
        },

        async deleteCategory(value: string) {
            try {
                await this.companyService.deleteCategory({ value });

                switch (this.categoryEditType) {
                    case businessCategoryTypes.tier1:
                        this.tier1Categories = this.tier1Categories.filter(item => item.value !== value);
                        this.tier1Selected = "";
                        this.tier2Categories = [];
                        this.tier2Selected = "";
                        this.tier3Categories = [];
                        this.tier3Selected = "";
                        break;

                    case businessCategoryTypes.tier2:
                        this.tier2Categories = this.tier2Categories.filter(item => item.value !== value);
                        this.tier2Selected = "";
                        this.tier3Categories = [];
                        this.tier3Selected = "";
                        break;

                    case businessCategoryTypes.tier3:
                        this.tier3Categories = this.tier3Categories.filter(item => item.value !== value);
                        this.tier3Selected = "";
                        break;

                    default:
                        break;
                }
                commonStore.showNotification("success", "Successfully removed");
            } catch (e) {
                showErrorNotification(e.code, "deleteCategory");
            }
        },

        async setCategoryEditType(type: BusinessCategoryType) {
            this.categoryEditType = type;
        },

        edit({ value, type, path }: BusinessCategory) {
            this.categoryEditMeta = {
                header: `Edit category "${value}"`,
                editMode: true,
                type,
                path,
            };
            this.categoryEditValue = value;
            this.categoryEditTarget = value;
            this.isEditModalOpen = true;
            this.categoryEditType = type;
        },

        async renameCategory() {
            try {
                const value = this.categoryEditValue;
                const target = this.categoryEditTarget;
                await this.companyService.renameCategory({
                    target,
                    value,
                });
                switch (this.categoryEditType) {
                    case businessCategoryTypes.tier1:
                        this.tier1Categories = this.tier1Categories.map((item: BusinessCategory) => item.value === target ? { ...item, value } : item);
                        this.tier1Selected = value;
                        break;

                    case businessCategoryTypes.tier2:
                        this.tier2Categories = this.tier2Categories.map((item: BusinessCategory) => item.value === target ? { ...item, value } : item);
                        this.tier2Selected = value;
                        break;

                    case businessCategoryTypes.tier3:
                        this.tier3Categories = this.tier3Categories.map((item: BusinessCategory) => item.value === target ? { ...item, value } : item);
                        this.tier3Selected = value;
                        break;

                    default:
                        break;
                }
                this.categoryEditTarget = "";
                this.categoryEditValue = "";
                this.isEditModalOpen = false;
                commonStore.showNotification("success", "Successfully renamed");
            } catch (e) {
                showErrorNotification(e.code, "renameCategory");
            }
        },

        async updateSearchForm(value: string) {
            this.searchForm = value;
            if (value.length >= 3) {
                const tier1Data =
                    await this.fetchData({
                        page: 1,
                        "per-page": 50,
                        full: true,
                        filter: businessCategoryTypes.tier1,
                        search: this.searchForm,
                        counters: !!this.searchForm,
                    });
                this.tier1Categories = tier1Data.data;
                this.tier1Meta = tier1Data.meta;
                if (this.tier1Selected) {
                    await this.setTier1(this.tier1Selected)
                }
                if (this.tier2Selected) {
                    await this.setTier2(this.tier2Selected)
                }
            } else if (value.length === 0) {
                const tier1Data =
                    await this.fetchData({
                        page: 1,
                        "per-page": 50,
                        full: true,
                        filter: businessCategoryTypes.tier1,
                    });
                this.tier1Categories = tier1Data.data;
                this.tier1Meta = tier1Data.meta;
                if (this.tier1Selected) {
                    await this.setTier1(this.tier1Selected)
                }
                if (this.tier2Selected) {
                    await this.setTier2(this.tier2Selected)
                }
            }
        },

        async refreshSearchForm() {
            this.searchForm = "";
            this.tier3Categories = [];
            this.tier2Categories = [];
            this.tier1Selected = "";
            this.tier2Selected = "";
            await this.init();
        }
    },
});