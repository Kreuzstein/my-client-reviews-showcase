import { RouteRecordRaw } from "vue-router";
import { routesNames } from "@/routesNames";
import HomePage from "@/pages/home/Container.vue";
import AccessForbidden from "@/pages/error/accessForbidden/Container.vue";
import AdminDashboardLayout from "@/admin/containers/dashboardLayout/AdminDashboardLayout.vue";
import AdminMain from "@/admin/containers/main/AdminMain.vue";
import AdminReportedReviews from "@/admin/containers/reportedReviews/AdminReportedReviews.vue";
import AdminPages from "@/admin/containers/pages/AdminPages.vue";
import AdminPagesList from "@/admin/containers/pages/AdminPagesList.vue";
import AdminEditPage from "@/admin/containers/pages/AdminEditPage.vue";
import AdminCallList from "@/admin/containers/callList/AdminCallList.vue";
import AdminCompanyReport from "@/admin/containers/companyReport/AdminCompanyReport.vue";
import AdminNewsletter from "@/admin/containers/newsletter/AdminNewsletter.vue";
import AdminNewsletterList from "@/admin/containers/newsletter/AdminNewsletterList.vue";
import AdminEditNewsletter from "@/admin/containers/newsletter/AdminEditNewsletter.vue";
import AdminBusinessCategories from "@/admin/containers/businessCategories/AdminBusinessCategories.vue";
import AdminCMSLayout from "@/admin/containers/cmsLayout/AdminCMSLayout.vue";
import AdminPaymentIssues from "@/admin/containers/paymentIssues/AdminPaymentIssues.vue";
import AdminReviewImport from "@/admin/containers/reviewImport/AdminReviewImport.vue";
// other container imports go here

export const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: routesNames.home,
        component: HomePage,
        meta: {
            title: "Genuine Reviews for Business",
        },
    },
    {
        path: "/error",
        name: routesNames.error,
        component: HomePage,
        meta: {
            title: "Error",
        },
    },
    {
        path: "/access-forbidden",
        name: routesNames.accessForbidden,
        component: AccessForbidden,
        meta: {
            title: "403 Access Forbidden",
        },
    },
    // Admin pages
    {
        path: "/admin",
        name: routesNames.adminDashboard,
        component: AdminDashboardLayout,
        meta: {
            requiresAuth: true,
        },
        redirect: { name: routesNames.adminDashboardMain },
        children: [
            {
                path: "main",
                name: routesNames.adminDashboardMain,
                component: AdminMain,
                meta: {
                    title: "Admin Main",
                },
            },
            {
                path: "reported-reviews",
                name: routesNames.adminDashboardReportedReviews,
                component: AdminReportedReviews,
                meta: {
                    title: "Admin Reported Reviews",
                },
            },
            {
                path: "call-list",
                component: AdminPages,
                children: [
                    {
                        path: "",
                        name: routesNames.adminDashboardCallList,
                        component: AdminCallList,
                        meta: {
                            title: "Admin Call List",
                        }
                    },
                    {
                        path: ":id",
                        name: routesNames.adminDashboardCompanyReport,
                        component: AdminCompanyReport,
                        meta: {
                            title: "Admin Company Report",
                        }
                    },
                ],
            },
            {
                path: "payment-issues",
                name: routesNames.adminDashboardPaymentIssues,
                component: AdminPaymentIssues,
                meta: {
                    title: "Payment Issues List"
                }
            },
            {
                path: "review-import",
                component: AdminReviewImport,
                name: routesNames.adminReviewImport,
                meta: {
                    requiresAuth: true,
                    title: "Review Import",
                },
            },
        ],
    },
    {
        path: "/cms",
        name: routesNames.adminCMS,
        component: AdminCMSLayout,
        meta: {
            requiresAuth: true,
        },
        redirect: { name: routesNames.adminDashboardPages },
        children: [
            {
                path: "pages",
                name: routesNames.adminDashboardPages,
                redirect: { name: routesNames.adminDashboardPagesList },
                component: AdminPages,
                children: [
                    {
                        path: "list",
                        name: routesNames.adminDashboardPagesList,
                        component: AdminPagesList,
                        meta: {
                            title: "Admin Pages List",
                        },
                    },
                    {
                        path: "edit/:id?",
                        name: routesNames.adminDashboardEditPage,
                        component: AdminEditPage,
                        meta: {
                            title: "Admin Edit Page",
                        },
                    },
                ],
            },
            {
                path: "newsletter",
                name: routesNames.adminNewsletter,
                redirect: { name: routesNames.adminNewsletterList },
                component: AdminNewsletter,
                children: [
                    {
                        path: "list",
                        name: routesNames.adminNewsletterList,
                        component: AdminNewsletterList,
                        meta: {
                            title: "Newsletter CMS",
                        },
                    },
                    {
                        path: "edit/:id?",
                        name: routesNames.adminEditNewsletter,
                        component: AdminEditNewsletter,
                        meta: {
                            title: "Newsletter CMS",
                        },
                    },
                ],
            },
            {
                path: "categories",
                name: routesNames.adminBusinessCategories,
                component: AdminBusinessCategories,
                meta: {
                    title: "Business Categories"
                }
            }
        ]
    },
    //   other route declarations go here
];