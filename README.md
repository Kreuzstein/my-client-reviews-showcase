# My Client Reviews (Showcase)

[My Client Reviews](https://myclientreviews.com.au/) is a review platform that tackles the issue of review bombing for reputation-sensitive businesses. Since we cannot share the MCR codebase publicly, this repository serves to showcase the project structure and technologies used with the help of code snippets and duds. 

## Tech Stack

### Client app
- Vue.js
- Vite-SSR
- SCSS
- Pinia 
- Yarn

### API app
- Node.js (TypeScript) 
- Fastify
- Pug templates
- Mongoose 
- Yarn

### Infra
- MongoDB (hosted on AWS EC2 VM)
- AWS ECS (production, using load balancer & a Linux bastion instance), AWS EC2 (UAT)
- BetterStack for outages
- AWS CloudWatch logs
- Github runners
- NginX
- Bash (DB backup automation)
- Slack bot integration with in-house monitoring

## Project Structure

```sh
/
├── docker-compose.yml <<<<<<<<<<<<<<<<<<<<<<<<<< Common compose file 
├── docker-compose.stage-id.yml <<<<<<<<<<<<<<<<< Stage-specific compose file
├── docs <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Documented DevOps and support processes
├── .github <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< GitHub runners definitions 
├── backend <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Fastify Node.js API
│   ├── Dockerfile <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< API app container definitions
│   ├── public-static
│   └── src 
│       ├── emails <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Email templates using Pug 
│       ├── helpers
│       ├── models <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Mongoose definitions for MongoDB
│       ├── routes                    ┐<<<<<<<<<< Fastify API endpoints
│       │   ├── admin                 │
│       │   ├── billing               │
│       │   ├── client                │
│       │   ├── common                │
│       │   ├── company               │
│       │   ├── newsletter            │
│       │   ├── page                  │
│       │   ├── review                │
│       │   ├── user                  │
│       │   └── widget                ┘
│       ├── schemas <<<<<<<<<<<<<<<<<<<<<<<<<<<< Adapters between Mongoose and JSON schemas  
│       ├── services
│       ├── static
│       └── types
└── frontend <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Vue.js Vite-SSR client app
    ├── Dockerfile <<<<<<<<<<<<<<<<<<<<<<<<<<<<< Client container definitions
    ├── public
    └── src
        ├── admin <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Components for the admin panel
        │   ├── components
        │   ├── containers
        │   │   ├── main
        │   │   ├── dashboardLayout    
        │   │   ├── cmsLayout          ┐<<<<<<<<< Built-in CMS
        │   │   ├── pages              │
        │   │   ├── businessCategories │
        │   │   ├── newsletter         ┘  
        │   │   ├── reportedReviews    ┐<<<<<<<<< Bans and abuse management
        │   │   ├── banModal           ┘
        │   │   ├── paymentIssues <<<<<<<<<<<<<<< Payment error tracker
        │   │   ├── reviewImport <<<<<<<<<<<<<<<< Importing reviews from other platforms
        │   │   ├── callList           ┐<<<<<<<<< Bespoke CRM & customer support
        │   │   ├── companyReport      │
        │   │   ├── saleReps           │
        │   │   ├── leadsList          │
        │   │   ├── leadReport         │
        │   │   └── salesLayout        ┘
        │   ├── helpers
        │   └── supervisor <<<<<<<<<<<<<<<<<<<<<< Layout wrappers for admins to telescope into other users
        ├── assets
        ├── components <<<<<<<<<<<<<<<<<<<<<<<<<< Shared UI atoms
        ├── containers
        │   ├── alertModal
        │   ├── companyDashboardLayout
        │   ├── emailTemplatesForm
        │   ├── helpPopup
        │   ├── inviteClients
        │   ├── mobileWarning
        │   ├── notificationModal
        │   ├── paymentModal
        │   ├── reviewModal
        │   └── widgetEditor <<<<<<<<<<<<<<<<<<<< Embeddable widgets
        ├── helpers
        ├── pages
        │   ├── company
        │   │   ├── addReview
        │   │   ├── edit
        │   │   ├── marketing
        │   │   ├── profile
        │   │   ├── search
        │   │   └── share-your-reviews
        │   ├── dashboard
        │   │   ├── getReviews
        │   │   ├── reviews
        │   │   └── stats
        │   ├── error
        │   ├── home
        │   ├── page
        │   ├── support
        │   ├── user
        │   │   ├── forgotPassword
        │   │   ├── login
        │   │   ├── logout
        │   │   ├── newPassword
        │   │   └── registration
        │   └── widget
        │       ├── editor
        │       └── view
        ├── server
        ├── services <<<<<<<<<<<<<<<<<<<<<<<<<<<< API endpoint integration
        ├── stores <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Common Pinia stores
        ├── styles
        └── types
```

## Licence
Property of Quorum IO Limited, all rights reserved. The public repo is for showcase purposes only. View LICENCE for further details.

## Contact
For any queries, feel free to reach us at [our official email](mailto:admin@quorum.ltd) or to me directly at [my official email](mailto:anton@quorum.ltd).